$(document).ready( function(){
    $('input:radio').click(function() {
        $("#firstCompetitorScore").prop("disabled",true);
        $("#secondCompetitorScore").prop("disabled",true);
        $("#firstCompetitorScore,#secondCompetitorScore").val("");
        if($(this).hasClass('exactScore')) {
            $("#firstCompetitorScore").prop("disabled",false);
            $("#secondCompetitorScore").prop("disabled",false);
        }
    });
});