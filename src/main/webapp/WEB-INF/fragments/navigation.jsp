<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}>
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery/set_formaction.js" type="text/javascript"></script>
</head>
<body>
    <form class="nav-form">
        <input type="hidden" value="${competition.id}" name="competitionID"/>
        <button class="nav-button" type="submit" formmethod="post" formaction="${pageContext.servletContext.contextPath}/controller?command=mainPage">
            <fmt:message key="navigation.main" />
        </button>
        <c:if test="${sessionScope.user.role eq 'ADMIN'}">
            <button class="nav-button" type="submit" formmethod="post" formaction="${pageContext.servletContext.contextPath}/controller?command=userManagement">
                <fmt:message key="navigation.userManagement"/>
            </button>
            <button class="nav-button" type="submit" formmethod="post" formaction="${pageContext.servletContext.contextPath}/controller?command=competitionCreatingPage">
                <fmt:message key="navigation.createCompetition"/>
            </button>
        </c:if>
        <button class="nav-button" id="en" type="submit" formmethod="post">
              English
        </button>
        <button class="nav-button" id="ru" type="submit" formmethod="post" >
              Русский
        </button>
        <button class="nav-button" type="submit" formmethod="post" formaction="${pageContext.servletContext.contextPath}/controller?command=signOut">
            <fmt:message key="navigation.signout" />
        </button>
    </form>
</body>
