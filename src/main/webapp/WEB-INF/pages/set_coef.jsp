
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>

    <meta http-equiv="Content-Language" content="en"/>
    <title>Set betEvents</title>
    <link rel="stylesheet" href="css/page-style.css">
    <link rel="stylesheet" href="css/set-up-coef.css">
</head>
<body>
<div class="container">
    <div class="nav">
        <jsp:include page="../fragments/navigation.jsp"/>
    </div>
    <div class="right-column">
        <div class="header">
            <jsp:include page="../fragments/header.jsp"/>
        </div>
        <div class="main">
            <div class="set-coef-container">
                <div class="title">
                    <span class="competitors">${competition.firstCompetitor} vs ${competition.secondCompetitor}</span>
                </div>
                <form class="input-form" id="betEvents" action="${pageContext.servletContext.contextPath}/controller?command=setUpCoefficients" method="post">
                    <input type="hidden" name="competitionID" value="${competition.id}"/>
                    <div class="input-row">
                        <span class="label"><fmt:message key="bet.win"/>${competition.firstCompetitor}</span>
                        <input class="input-field" type="text" maxlength = "7" required pattern="\d{1,4}\.\d{2}" name="winFirst"/>
                    </div>
                    <div class="input-row">
                        <span class="label"><fmt:message key="bet.draw"/> </span>
                        <input class="input-field" type="text" maxlength = "7" required pattern="\d{1,4}\.\d{2}" name="draw"/>
                    </div>
                    <div class="input-row">
                        <span class="label"><fmt:message key="bet.win"/> ${competition.secondCompetitor}</span>
                        <input class="input-field" type="text" maxlength = "7" required pattern="\d{1,4}\.\d{2}" name="winSecond"/>
                    </div>
                    <div class="input-row">
                        <span class="label"><fmt:message key="bet.exactScore"/></span>
                        <input class="input-field" type="text" maxlength = "7" required pattern="\d{1,4}\.\d{2}" name="exactScore"/>
                    </div>

                    <button class="button"><fmt:message key="setup"/></button>

                </form>

            </div>
        </div>
    </div>
</div>
</body>
</html>