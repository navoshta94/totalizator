<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

    <meta http-equiv="Content-Language" content="en"/>
    <title>Create competition</title>
    <link rel="stylesheet" href="css/page-style.css">
    <link rel="stylesheet" href="css/competition-form-style.css">
</head>
<body>
<div class="container">
    <div class="nav">
        <jsp:include page="../fragments/navigation.jsp"/>
    </div>
    <div class="right-column">
        <div class="header">
            <jsp:include page="../fragments/header.jsp"/>
        </div>
        <div class="main">
            <form class="input-form" action="${pageContext.servletContext.contextPath}/controller?command=competitionCreating" method="post">
                <span class="label"><fmt:message key="competition.startdate"/></span>
                <input class="input-field" type="text" size="12" required pattern="\d{2}\.\d{2}\.\d{4}"
                       placeholder=<fmt:message key="competition.startdate.placeholder"/> name="startDate"/>
                <span class="label"> <fmt:message key="competition.starttime"/></span>
                <input class="input-field" type="text" size="12" required pattern="\d{2}:\d{2}"
                       placeholder=<fmt:message key="competition.starttime.placeholder"/> name="startTime"/>
                <span class="label"><fmt:message key="competition.firstCompetitor"/></span>
                <input class="input-field" type="text" name="first_competitor"  required pattern="\w+"/>
                <span class="label"><fmt:message key="competition.secondCompetitor"/></span>
                <input class="input-field" type="text" name="second_competitor"  required pattern="\w+"/>
                <button class="button"><fmt:message key="competition.create"/></button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
