<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html>
<head>
    <meta charset="UTF-8">
    <title>Bets</title>
    <link rel="stylesheet" href="css/page-style.css">
    <link rel="stylesheet" href="css/make-bet-style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery/make_bet_form.js" type="text/javascript"></script>
    <c:set var="winFirstCompetitor" value="WIN_FIRST_COMPETITOR"/>
    <c:set var="draw" value="DRAW"/>
    <c:set var="winSecondCompetitor" value="WIN_SECOND_COMPETITOR"/>
    <c:set var="exactScore" value="EXACT_SCORE"/>

</head>
<body>
    <div class="container">
        <div class="nav">
            <jsp:include page="../fragments/navigation.jsp"/>
        </div>
        <div class="right-column">
            <div class="header">
                <jsp:include page="../fragments/header.jsp"/>
            </div>
            <div class="main">
                <form class="make-bet" action="${pageContext.servletContext.contextPath}/controller?command=makeBet" method="POST">
                    <div class="info">
                        <span class="label"><fmt:message key="bet.win"/> ${requestScope.competition.firstCompetitor}:</span>
                        <c:forEach items="${requestScope.betEvents}" var="betEvent">
                            <span class="label">
                                <c:if test="${betEvent.eventType eq winFirstCompetitor}">
                                    <c:set var="firstWinCoef" value="${betEvent.coefficient}"></c:set>
                                    ${betEvent.coefficient}
                                </c:if>
                            </span>
                        </c:forEach>
                    </div>
                    <div class="info">
                        <span class="label"><fmt:message key="bet.draw"/>:</span>
                        <c:forEach items="${requestScope.betEvents}" var="betEvent">
                            <span class="label">
                                <c:if test="${betEvent.eventType eq draw}">
                                    <c:set var="drawCoef" value="${betEvent.coefficient}"></c:set>
                                    ${betEvent.coefficient}
                                </c:if>
                            </span>
                        </c:forEach>
                    </div>
                    <div class="info">
                        <span class="label"><fmt:message key="bet.win"/>${requestScope.competition.secondCompetitor}:</span>
                        <c:forEach items="${requestScope.betEvents}" var="betEvent">
                            <span class="label">
                                <c:if test="${betEvent.eventType eq winSecondCompetitor}">
                                    <c:set var="secondWinCoef" value="${betEvent.coefficient}"></c:set>
                                    ${betEvent.coefficient}
                                </c:if>
                        </c:forEach>
                    </div>
                    <div class="info">

                        <span class="label"><fmt:message key="bet.exactScore"/>:</span>
                        <c:forEach items="${requestScope.betEvents}" var="betEvent">
                        <span class="label">
                                <c:if test="${betEvent.eventType eq exactScore}">
                                    <c:set var="exactScoreCoef" value="${betEvent.coefficient}"></c:set>
                                    ${betEvent.coefficient}
                                </c:if>
                        </c:forEach>
                    </div>
                    <div class="bet-type">
                        <input type="radio" name="eventType" value="${winFirstCompetitor}" checked="checked"><fmt:message key="bet.win"/>${requestScope.competition.firstCompetitor}</input>
                        <input type="radio" name="eventType" value="${draw}"><fmt:message key="bet.draw"/></input>
                        <input type="radio" name="eventType" value="${winSecondCompetitor}"><fmt:message key="bet.win"/>${requestScope.competition.secondCompetitor}</input>
                        <input type="radio" name="eventType" value="${exactScore}" class="exactScore" id="exactResult" ><fmt:message key="bet.exactScore"/></input>
                    </div>
                    <div class="exact-scores">
                        <div class="input-field">
                            <span class="label">${requestScope.competition.firstCompetitor} <fmt:message key="bet.score"/></span>
                            <input type="text" class="input-score" name="firstCompetitorScore" id="firstCompetitorScore" maxlength = "3" disabled="true" />
                        </div>
                        <div class="input-field">
                            <span class="label">${requestScope.competition.secondCompetitor} <fmt:message key="bet.score"/></span>
                            <input type="text" class="input-score" name="secondCompetitorScore" id="secondCompetitorScore" maxlength = "3" disabled="true" />
                        </div>
                    </div>
                    <input type="text" class="bet-amount" name="betAmount" maxlength = "10"/>

                    <button name="competitionID" value="${requestScope.competition.id}" class="button"><fmt:message key="bet.makeBet"/></button>
                    <div class="message">
                        <c:if test="${not empty message}">
                            <fmt:message key="${requestScope.message}"/>
                        </c:if>
                    </div>

                </form>
            </div>
        </div>
    </div>
</body>
</html>
