<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Bets</title>
    <link rel="stylesheet" href="css/main-styles.css">
    <link rel="stylesheet" href="css/page-style.css">
</head>
<body>
    <div class="container">
        <div class="nav">
            <jsp:include page="../fragments/navigation.jsp"/>
        </div>
        <div class="right-column">
            <div class="header">
                <jsp:include page="../fragments/header.jsp"/>
            </div>
            <div class="main">
                <div class="content">
                    <div class="title">
                        <span class="beginning"><fmt:message key="main.beginning" /></span>
                        <span class="competitors"><fmt:message key="main.competitors"/></span>
                        <span class="buttons"></span>
                    </div>
                    <div class="competitions">
                        <c:forEach items="${competitionsList}" var="competition">
                            <div class="competition">
                                <span class="beginning"><javatime:format value="${competition.beginning}" style="MS"/></span>
                                <span class="competitors">${competition.firstCompetitor} vs ${competition.secondCompetitor}</span>
                                <div class="buttons">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.role eq 'CLIENT'}">
                                            <form action="${pageContext.servletContext.contextPath}/controller?command=makeBetPage" method="POST">
                                                <button class="button" name="competitionID" value="${competition.id}"><fmt:message key="main.makebet" /></button>
                                            </form>
                                        </c:when>
                                        <c:when test="${sessionScope.user.role eq 'ADMIN'}">
                                            <form action="${pageContext.servletContext.contextPath}/controller?command=competitionEditingPage" method="POST">
                                                <button class="button" name="competitionID" value="${competition.id}"><fmt:message key="main.edit" /></button>
                                            </form>
                                            <form action="${pageContext.servletContext.contextPath}/controller?command=setCompetitionResult" method="POST">
                                                <button class="button" name="competitionID" value="${competition.id}"><fmt:message key="main.set.result" /></button>
                                            </form>
                                        </c:when>
                                        <c:when test="${sessionScope.user.role eq 'BOOKMAKER'}">
                                            <form action="${pageContext.servletContext.contextPath}/controller?command=setUpCoefPage" method="POST">
                                                <button class="button" name="competitionID" value="${competition.id}"><fmt:message key="main.setUpCoefficients"/></button>
                                            </form>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <c:if test="${not empty message}">
                        <div class="message">
                            <span><fmt:message key="${requestScope.message}"/></span>
                        </div>
                    </c:if>
                </div>
            </div>

        </div>
    </div>
</body>
</html>
