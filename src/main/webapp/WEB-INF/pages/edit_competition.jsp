<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="javatime" uri="http://sargue.net/jsptags/time" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title>Editing page</title>
    <link rel="stylesheet" href="css/page-style.css">
    <link rel="stylesheet" href="css/competition-form-style.css">
</head>
<body>
<div class="container">
    <div class="nav">
        <jsp:include page="../fragments/navigation.jsp"/>
    </div>
    <div class="right-column">
        <div class="header">
            <jsp:include page="../fragments/header.jsp"/>
        </div>
        <div class="main">
            <form class="input-form" action="${pageContext.servletContext.contextPath}/controller?command=editCompetition" method="post">
                <span class="label"><fmt:message key="competition.startdate" /> </span>
                <input class="input-field" type="text" name="startDate"
                       value=<javatime:format pattern="dd.MM.yyyy" value="${competition.beginning}" style="MS"/>
                               <span class="label"> <fmt:message key="competition.startdate" /> </span>
                <input class="input-field" type="text" name="startTime"
                       value=<javatime:format pattern="HH:mm" value="${competition.beginning}" style="MS"/>
                               <span class="label"><fmt:message key="competition.starttime" /></span>
                <input class="input-field" type="text" name="first_competitor" value="${requestScope.competition.firstCompetitor}"/>
                <span class="label"><fmt:message key="competition.secondCompetitor" /></span>
                <input class="input-field" type="text" name="second_competitor" value="${requestScope.competition.secondCompetitor}"/>
                <button class="button" name="id" value="${requestScope.competition.id}"><fmt:message key="competition.edit" /></button>
            </form>
        </div>
    </div>
</div>
</body>
</html>


