<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title>Log in</title>
    <link href="css/login-style.css" rel="stylesheet" type="text/css">
</head>
<body>

    <div class="login-page">
        <div class="form">
            <form action="${pageContext.servletContext.contextPath}/controller?command=login" method="post" class="login-form">
                <input type="text" name="login" placeholder=<fmt:message key="login.username"/> required pattern="\w+" />

                <input type="password" name="password" placeholder=<fmt:message key="login.password"/> required pattern="*+" />

                <button><fmt:message key="login.submit"/></button>
            </form>
            <div class="message">
                <div class="error">
                    <c:if test="${not empty errorMessage}">
                        <fmt:message key="${errorMessage}"/>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</body>
</html>