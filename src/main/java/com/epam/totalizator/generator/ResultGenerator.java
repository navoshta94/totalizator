package com.epam.totalizator.generator;

import com.epam.totalizator.entity.Result;

public class ResultGenerator {
    private static final Integer MAX_SCORE = 10;
    public Result generate(Long competitionID) {
        return new Result(competitionID, generateScore(), generateScore());
    }

    private Integer generateScore() {
        return (int) (Math.random() * MAX_SCORE);
    }
}
