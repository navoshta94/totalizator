package com.epam.totalizator.builder;

import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.entity.user.UserRole;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserBuilder implements Builder<User>{

    @Override
    public List<User> build(ResultSet resultSet) throws SQLException {
        List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(User.ID);
            String login = resultSet.getString(User.LOGIN);
            String password = resultSet.getString(User.PASSWORD);
            String role = resultSet.getString(User.ROLE).toUpperCase();
            Boolean isActive = resultSet.getBoolean(User.IS_ACTIVE);
            BigDecimal cashAmount = resultSet.getBigDecimal(User.CASH_AMOUNT);
            userList.add(new User(id, login, password, UserRole.valueOf(role), cashAmount, isActive));
        }
        return userList;
    }
}
