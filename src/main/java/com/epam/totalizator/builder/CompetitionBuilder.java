package com.epam.totalizator.builder;

import com.epam.totalizator.entity.Competition;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CompetitionBuilder implements Builder<Competition> {

    @Override
    public List<Competition> build(ResultSet resultSet) throws SQLException {
        List<Competition> competitions = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(Competition.ID);
            String firstCompetitor = resultSet.getString(Competition.FIRST_COMPETITOR);
            String secondCompetitor = resultSet.getString(Competition.SECOND_COMPETITOR);
            Timestamp beginning = resultSet.getTimestamp(Competition.COMPETITION_BEGINNING);

            competitions.add(new Competition(id, firstCompetitor, secondCompetitor, beginning.toLocalDateTime()));
        }
        return competitions;
    }
}
