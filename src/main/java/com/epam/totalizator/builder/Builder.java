package com.epam.totalizator.builder;

import com.epam.totalizator.entity.Identifable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Builder<T> {

    List<T> build(ResultSet resultSet) throws SQLException;
}
