package com.epam.totalizator.builder;

import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.EventType;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BetEventBuilder implements Builder<BetEvent> {

    @Override
    public List<BetEvent> build(ResultSet resultSet) throws SQLException {
        List<BetEvent> betEvents = new ArrayList<>();
        while (resultSet.next()) {
            Long competitionID = resultSet.getLong(BetEvent.COMPETITION_ID);
            EventType eventType = EventType.
                    valueOf(resultSet.getString(BetEvent.EVENT_TYPE).toUpperCase());
            BigDecimal coefficient = resultSet.getBigDecimal(BetEvent.COEFFICIENT);
            betEvents.add(createCoefficient(competitionID, eventType, coefficient));

        }
        return betEvents;
    }

    private BetEvent createCoefficient(Long competitionID,
                                       EventType eventType,
                                       BigDecimal coefficient) {

        switch (eventType) {
            case WIN_FIRST_COMPETITOR:
                return BetEvent.firsCompetitorWin(competitionID, coefficient);
            case DRAW:
                return BetEvent.draw(competitionID, coefficient);
            case WIN_SECOND_COMPETITOR:
                return BetEvent.secondCompetitorWin(competitionID, coefficient);
            case EXACT_SCORE:
                return BetEvent.exactScore(competitionID, coefficient);

                default: throw new IllegalStateException();
        }
    }
}
