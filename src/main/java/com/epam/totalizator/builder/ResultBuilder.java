package com.epam.totalizator.builder;

import com.epam.totalizator.entity.Result;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultBuilder implements Builder<Result>{

    @Override
    public List<Result> build(ResultSet resultSet) throws SQLException {
        List<Result> resultList = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(Result.ID);
            Long competitionID = resultSet.getLong(Result.COMPETITION_ID);
            Integer firstCompetitorScore = resultSet.getInt(Result.FIRST_COMPETITOR_SCORE);
            Integer secondCompetitorScore = resultSet.getInt(Result.SECOND_COMPETITOR_SCORE);
            resultList.add(new Result(id, competitionID, firstCompetitorScore, secondCompetitorScore));
        }
        return resultList;
    }
}
