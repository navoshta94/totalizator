package com.epam.totalizator.builder;

import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.entity.bet.BetKind;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BetBuilder implements Builder<Bet> {
    @Override
    public List<Bet> build(ResultSet resultSet) throws SQLException {
        List<Bet> bets = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(Bet.ID);
            Long competitionID = resultSet.getLong(Bet.COMPETITION_ID);
            Long userID = resultSet.getLong(Bet.USER_ID);
            Boolean firstWin = resultSet.getBoolean(Bet.FIRST_WIN);
            Boolean draw = resultSet.getBoolean(Bet.DRAW);
            Boolean secondWin = resultSet.getBoolean(Bet.SECOND_WIN);
            Integer firstCompetitorScore = resultSet.getInt(Bet.FIRST_COMPETITOR_SCORE);
            Integer secondCompetitorScore = resultSet.getInt(Bet.SECOND_COMPETITOR_SCORE);
            BetKind betKind = BetKind.valueOf(resultSet.getString(Bet.BET_KIND).toUpperCase());
            BigDecimal betAmount = resultSet.getBigDecimal(Bet.BET_AMOUNT);
            BigDecimal winBetAmount = resultSet.getBigDecimal(Bet.WIN_BET_AMOUNT);
            Boolean isWin = resultSet.getBoolean(Bet.IS_WIN);

            bets.add(new Bet(id, competitionID, userID, firstWin, draw, secondWin, firstCompetitorScore,
                    secondCompetitorScore, betKind, betAmount, winBetAmount, isWin));
        }
        return bets;
    }
}
