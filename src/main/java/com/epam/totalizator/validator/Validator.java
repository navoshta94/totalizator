package com.epam.totalizator.validator;

/**
 * Created by e.navosha on 24.01.19.
 */
public class Validator {
    private static final String ID_PATTERN = "\\d{1,17}";

    public static boolean validateID(String id) {
        if (id != null && id.matches(ID_PATTERN)) {
            return true;
        }
        return false;
    }
}
