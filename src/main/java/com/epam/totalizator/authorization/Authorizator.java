package com.epam.totalizator.authorization;

import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.entity.user.UserRole;

import java.util.Arrays;
import java.util.List;

public class Authorizator {
    private List<String> clientCommands;
    private List<String> adminCommands;
    private List<String> bookmakerCommands;

    public Authorizator() {
        clientCommands = Arrays.asList(CommandFactory.LOGIN_PAGE,
                                        CommandFactory.LOGIN,
                                        CommandFactory.MAIN_PAGE,
                                        CommandFactory.MAKE_BET,
                                        CommandFactory.MAKE_BET_PAGE,
                                        CommandFactory.SIGN_OUT);
        adminCommands = Arrays.asList(CommandFactory.LOGIN_PAGE,
                                        CommandFactory.LOGIN,
                                        CommandFactory.MAIN_PAGE,
                                        CommandFactory.USER_MANAGEMENT_PAGE,
                                        CommandFactory.CHANGE_USER_STATUS,
                                        CommandFactory.CREATE_COMPETITION_PAGE,
                                        CommandFactory.CREATE_COMPETITION_PAGE,
                                        CommandFactory.CREATE_COMPETITION,
                                        CommandFactory.CREATE_COMPETITION_PAGE,
                                        CommandFactory.EDIT_COMPETITION_PAGE,
                                        CommandFactory.EDIT_COMPETITION,
                                        CommandFactory.SIGN_OUT,
                                        CommandFactory.SET_RESULT,
                                        CommandFactory.SET_BET_RESULT);
        bookmakerCommands = Arrays.asList(CommandFactory.LOGIN_PAGE,
                                            CommandFactory.LOGIN,
                                            CommandFactory.MAIN_PAGE,
                                            CommandFactory.SET_UP_COEFFICIENTS_PAGE,
                                            CommandFactory.SET_UP_COEFFICIENTS,
                                            CommandFactory.SIGN_OUT);
    }

    public boolean isHaveAccess(User user, String command) {
        UserRole userRole = user.getRole();
        if (userRole == UserRole.ADMIN) {
            return adminCommands.contains(command);
        } else if (userRole == UserRole.BOOKMAKER) {
            return bookmakerCommands.contains(command);
        } else if (userRole == UserRole.CLIENT) {
            return clientCommands.contains(command);
        }
        return false;
    }
}
