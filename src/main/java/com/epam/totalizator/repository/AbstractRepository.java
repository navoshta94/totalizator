package com.epam.totalizator.repository;

import com.epam.totalizator.builder.Builder;
import com.epam.totalizator.entity.Identifable;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.helper.QueryHelper;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public abstract class AbstractRepository<T extends Identifable> implements Repository<T>{
    private Connection connection;


    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public List<T> executeQuery(String query, Builder<T> builder,
                                List<String> params) throws RepositoryException {
        List<T> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (int i = 0; i < params.size(); i++) {
                preparedStatement.setObject(i + 1, params.get(i));
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            entities = builder.build(resultSet);
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
        return entities;
    }

    public void save(T item) throws RepositoryException {
        Map<String, Object> fields = getFields(item);
        String tableName = getTableName();
        item.getId();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement(QueryHelper.makeSaveByIDQuery(fields, tableName, item.getId()));
            List<Object> values = new ArrayList<>(fields.values());
            for (int i = 0; i < values.size(); i++) {
                preparedStatement.setObject(i+1, values.get(i));
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
    }


    public void add(T item) throws RepositoryException {

        Map<String, Object> fields = getFields(item);
        String tableName = getTableName();
        try{
            PreparedStatement preparedStatement = connection.
                    prepareStatement(QueryHelper.makeAddQuery(fields, tableName));
            List<Object> values = new ArrayList<>(fields.values());
            for (int i = 0; i < values.size(); i++) {
                preparedStatement.setObject(i+1, values.get(i));
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
    }

    protected abstract Map<String, Object> getFields(T item);
    protected abstract String getTableName();

}
