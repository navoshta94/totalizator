package com.epam.totalizator.repository.specification;

import java.util.Collections;
import java.util.List;

public class FindAllSpecification implements Specification {

    @Override
    public String toSql() {
        return ";";
    }

    @Override
    public List<Object> getParameters() {
        return Collections.emptyList();
    }
}
