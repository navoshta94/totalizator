package com.epam.totalizator.repository.specification;

import com.epam.totalizator.entity.bet.Bet;

import java.util.Arrays;
import java.util.List;

public class BetsOfCompetitionByKind implements Specification<Bet>{
    /*private Long competitionId;
    private Bet betKind;
    public BetsOfCompetitionByKind(Long competitionId, Bet betKind) {
        this.competitionId = competitionId;
        this.betKind = betKind;
    }*/
    @Override
    public String toSql() {
        return " where fk_competition_id = ? and bet_kind = ? order by first_competitor_score";
    }

    @Override
    public List<Object> getParameters() {
        //return Arrays.asList(competitionId, betKind.name());
        return null;
    }
}
