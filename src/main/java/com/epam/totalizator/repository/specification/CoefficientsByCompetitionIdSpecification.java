package com.epam.totalizator.repository.specification;

import com.epam.totalizator.entity.BetEvent;

import java.util.Arrays;
import java.util.List;

public class CoefficientsByCompetitionIdSpecification implements Specification<BetEvent>{
    private Long competitionID;

    public CoefficientsByCompetitionIdSpecification(Long competitionID) {
        this.competitionID = competitionID;
    }

    @Override
    public String toSql() {
        return " where fk_competition_id=?";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(competitionID);
    }
}
