package com.epam.totalizator.repository.specification;

import java.util.Arrays;
import java.util.List;

public class FindResultByCompetitionID implements Specification{
    private long competitionID;

    public FindResultByCompetitionID(long competitionID) {
        this.competitionID = competitionID;
    }

    @Override
    public String toSql() {
        return "where fk_competition_id = ?";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(competitionID);
    }
}
