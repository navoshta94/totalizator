package com.epam.totalizator.repository.specification;

import com.epam.totalizator.entity.user.User;

import java.util.Arrays;
import java.util.List;

public class UserByLoginAndPasswordSpecification implements Specification<User> {
    private String login;
    private String password;

    public UserByLoginAndPasswordSpecification(String login, String password) {
        this.login = login;
        this.password = password;
    }
    @Override
    public String toSql() {
        return "where login = ? and password = md5(?)";
    }//md5

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(login, password);
    }
}
