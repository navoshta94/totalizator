package com.epam.totalizator.repository.specification;

import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.EventType;

import java.util.Arrays;
import java.util.List;

public class BetEventByTypeSpecification implements Specification<BetEvent> {
    private EventType eventType;

    public BetEventByTypeSpecification(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toSql() {
        return "where event_type = ?";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(eventType.name());
    }
}
