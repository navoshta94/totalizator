package com.epam.totalizator.repository.specification;

import com.epam.totalizator.entity.Competition;

import java.util.Arrays;
import java.util.List;

public class FindByIdSpecification implements Specification {
    private long id;

    public FindByIdSpecification(long id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return "where id = ?";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }
}
