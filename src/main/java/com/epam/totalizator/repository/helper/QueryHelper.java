package com.epam.totalizator.repository.helper;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class QueryHelper{

    public static String makeSaveByIDQuery(Map<String, Object> fields, String table, long id) {
        StringBuilder stringBuilder = new StringBuilder("update " + table + " set ");
        Set<Map.Entry<String, Object>> entries = fields.entrySet();
        Iterator<Map.Entry<String
                , Object>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> pair = iterator.next();
            if (iterator.hasNext()) {
                stringBuilder.append(pair.getKey() + "=" + "?,");
            } else {
                stringBuilder.append(pair.getKey() + "=" + "? ");
            }
        }

        stringBuilder.append("where " + "id=" + id);
        return stringBuilder.toString();
    }

    public static String makeAddQuery(Map<String, Object> fields, String table) {
        StringBuilder stringBuilder = new StringBuilder("insert into " + table + "(");
        Set<String> fieldsName = fields.keySet();
        for (String field : fieldsName) {
            stringBuilder.append(field + ",");
        }
        stringBuilder.replace(stringBuilder.length() - 1, stringBuilder.length(), ") ");
        stringBuilder.append("values(");
        for (int i = 0; i < fieldsName.size(); i++) {
            stringBuilder.append("?,");
        }
        stringBuilder.replace(stringBuilder.length() - 1, stringBuilder.length(), ")");
        return stringBuilder.toString();
    }
}
