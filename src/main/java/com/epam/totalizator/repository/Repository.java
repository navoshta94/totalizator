package com.epam.totalizator.repository;

import com.epam.totalizator.entity.Identifable;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends Identifable> {
    void add(T object) throws RepositoryException;
    Optional<T> querySingleResult(Specification specification) throws RepositoryException;
    List<T> query(Specification specification) throws RepositoryException;
    void save(T t) throws RepositoryException;
    void delete(Specification specification) throws RepositoryException;
}
