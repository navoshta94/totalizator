package com.epam.totalizator.repository;

import com.epam.totalizator.builder.BetEventBuilder;
import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BetEventRepository extends AbstractRepository<BetEvent> {
    private final static String TABLE_NAME = "bet_event";

    public BetEventRepository(Connection connection) {
        super(connection);
    }

    @Override
    public Optional<BetEvent> querySingleResult(Specification specification) throws RepositoryException {
        String query = " select * from bet_event " + specification.toSql();
        List<BetEvent> betEvent = executeQuery(query, new BetEventBuilder(), specification.getParameters());
        if (!betEvent.isEmpty()) {
            return Optional.of(betEvent.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<BetEvent> query(Specification specification) throws RepositoryException {
        String query = "select * from bet_event " + specification.toSql();
        List<BetEvent> competitions = executeQuery(query, new BetEventBuilder(), specification.getParameters());
        return competitions;
    }

    @Override
    public void delete(Specification specification) throws RepositoryException {

    }

    @Override
    public void save(BetEvent item) throws RepositoryException {
        super.save(item);
    }

    @Override
    public void add(BetEvent item) throws RepositoryException {
        super.add(item);
    }

    @Override
    protected Map<String, Object> getFields(BetEvent item) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(BetEvent.COMPETITION_ID, item.getCompetitionID());
        fields.put(BetEvent.EVENT_TYPE, item.getEventType().name());
        fields.put(BetEvent.COEFFICIENT, item.getCoefficient());
        return fields;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
