package com.epam.totalizator.repository;

import com.epam.totalizator.builder.UserBuilder;
import com.epam.totalizator.entity.Identifable;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> {
    private static final String TABLE_NAME = "user";

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Map<String, Object> getFields(User item) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(User.LOGIN, item.getLogin());
        fields.put(User.PASSWORD, item.getPassword());
        fields.put(User.CASH_AMOUNT, item.getCashAmmount());
        fields.put(User.ROLE, item.getRole().name());
        fields.put(User.IS_ACTIVE, item.getIsActive());
        return fields;
    }


    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }



    @Override
    public Optional querySingleResult(Specification specification) throws RepositoryException {
        String query = " select * from user " + specification.toSql();
        List<User> users = executeQuery(query, new UserBuilder(), specification.getParameters());
        if (!users.isEmpty()) {
            return Optional.of(users.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<User> query(Specification specification) throws RepositoryException {
        String query = "select * from user " + specification.toSql();
        List<User> users = executeQuery(query, new UserBuilder(), specification.getParameters());
        return users;
    }

    @Override
    public void delete(Specification specification) {

    }
}
