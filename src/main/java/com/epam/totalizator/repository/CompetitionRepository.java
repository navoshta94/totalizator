package com.epam.totalizator.repository;

import com.epam.totalizator.builder.CompetitionBuilder;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.*;

public class CompetitionRepository extends AbstractRepository<Competition> {
    private final static String TABLE_NAME = "competition";

    public CompetitionRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Map<String, Object> getFields(Competition item) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Competition.COMPETITION_BEGINNING, Timestamp.valueOf(item.getBeginning()));
        fields.put(Competition.FIRST_COMPETITOR, item.getFirstCompetitor());
        fields.put(Competition.SECOND_COMPETITOR, item.getSecondCompetitor());
        return fields;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public void save(Competition object) throws RepositoryException {
        super.save(object);
    }

    @Override
    public void delete(Specification specification) throws RepositoryException {

    }

    @Override
    public void add(Competition object) throws RepositoryException {
        super.add(object);
    }

    @Override
    public Optional<Competition> querySingleResult(Specification specification) throws RepositoryException {
        String query = " select * from competition " + specification.toSql();
        List<Competition> competitions = executeQuery(query, new CompetitionBuilder(), specification.getParameters());
        if (!competitions.isEmpty()) {
            return Optional.of(competitions.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<Competition> query(Specification specification) throws RepositoryException {
        String query = "select * from competition " + specification.toSql();
        List<Competition> competitions = executeQuery(query, new CompetitionBuilder(), specification.getParameters());
        return competitions;
    }

}
