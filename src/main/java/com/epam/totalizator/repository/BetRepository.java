package com.epam.totalizator.repository;

import com.epam.totalizator.builder.BetBuilder;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.util.*;

public class BetRepository extends AbstractRepository<Bet> {
    private static final String TABLE_NAME = "bet";

    public BetRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Map<String, Object> getFields(Bet item) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Bet.COMPETITION_ID, item.getCompetitionID());
        fields.put(Bet.USER_ID, item.getUserID());
        fields.put(Bet.FIRST_WIN, item.getFirstWin());
        fields.put(Bet.DRAW, item.getDraw());
        fields.put(Bet.SECOND_WIN, item.getSecondWin());
        fields.put(Bet.FIRST_COMPETITOR_SCORE, item.getFirstCompetitorScore());
        fields.put(Bet.SECOND_COMPETITOR_SCORE, item.getSecondCompetitorScore());
        fields.put(Bet.BET_KIND, item.getBetKind().name());
        fields.put(Bet.BET_AMOUNT, item.getBetAmount());
        fields.put(Bet.WIN_BET_AMOUNT, item.getWinBetAmount());
        fields.put(Bet.IS_WIN, item.getWin());

        return fields;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Optional<Bet> querySingleResult(Specification specification) throws RepositoryException {
        String query = " select * from bet " + specification.toSql();
        List<Bet> competitions = executeQuery(query, new BetBuilder(), specification.getParameters());
        if (!competitions.isEmpty()) {
            return Optional.of(competitions.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<Bet> query(Specification specification) throws RepositoryException {
        String query = " select * from bet " + specification.toSql();
        List<Bet> bets = executeQuery(query, new BetBuilder(), specification.getParameters());
        if (bets.isEmpty()) {
            return Collections.emptyList();
        } else {
            return bets;
        }

    }

    @Override
    public void delete(Specification specification) throws RepositoryException {

    }
}
