package com.epam.totalizator.repository;

import com.epam.totalizator.exceptions.ConnectionPoolException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.jdbc.ConnectionPool;

import java.sql.Connection;

public class RepositoryFactory implements AutoCloseable{
    private Connection connection;

    public RepositoryFactory() throws RepositoryFactoryException{
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            this.connection = connectionPool.getConnection();
        } catch (ConnectionPoolException e) {
            throw new RepositoryFactoryException(e.getMessage(), e);
        }
    }

    public UserRepository getUserRepository() {
        return new UserRepository(connection);
    }

    public BetRepository getBetRepository() {return new BetRepository(connection);}

    public CompetitionRepository getCompetitionRepository() {
        return new CompetitionRepository(connection);
    }

    public BetEventRepository getCoefficientRepository() {return new BetEventRepository(connection); }

    public ResultRepository getResultRepository() {return new ResultRepository(connection);}
    @Override
    public void close(){
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.returnConnection(connection);
    }
}
