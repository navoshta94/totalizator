package com.epam.totalizator.repository;

import com.epam.totalizator.builder.ResultBuilder;
import com.epam.totalizator.entity.Result;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.repository.specification.Specification;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ResultRepository extends AbstractRepository<Result>{
    private static final String TABLE_NAME = "result";

    public ResultRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Map<String, Object> getFields(Result item) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Result.COMPETITION_ID, item.getCompetitionID());
        fields.put(Result.FIRST_COMPETITOR_SCORE, item.getFirstCompetitorScore());
        fields.put(Result.SECOND_COMPETITOR_SCORE, item.getSecondCompetitorScore());
        return fields;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Optional<Result> querySingleResult(Specification specification) throws RepositoryException {
        String query = " select * from " + TABLE_NAME + " " + specification.toSql();
        List<Result> competitions = executeQuery(query, new ResultBuilder(),
                specification.getParameters());
        if (!competitions.isEmpty()) {
            return Optional.of(competitions.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<Result> query(Specification specification) throws RepositoryException {
        return null;
    }

    @Override
    public void delete(Specification specification) throws RepositoryException {

    }
}
