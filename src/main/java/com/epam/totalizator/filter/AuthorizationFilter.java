package com.epam.totalizator.filter;

import com.epam.totalizator.authorization.Authorizator;
import com.epam.totalizator.entity.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationFilter implements Filter {
    private final static Logger logger = LogManager.getLogger(AuthorizationFilter.class);
    private final static String LOGIN_PAGE = "/totalizator/controller?command=loginPage";
    private Authorizator authorizator = new Authorizator();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        String command = req.getParameter("command");
        User user = (User)session.getAttribute("user");
        boolean isLogged = session.getAttribute("user") != null;
        if (!isLogged && !command.equals("login")) {
            resp.sendRedirect(LOGIN_PAGE);
        } else if (isLogged) {
            if(!authorizator.isHaveAccess(user, command)) {
                logger.warn("User:" + user +". Tried to have access:" + command);
                resp.sendRedirect(LOGIN_PAGE);
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
