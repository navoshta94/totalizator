package com.epam.totalizator.jdbc;

import com.epam.totalizator.exceptions.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private final static Logger logger = LogManager.getLogger(ConnectionPool.class);
    private static final int CONNECTIONS_AMOUNT = 16;
    private static ConnectionPool instance;
    private static Lock instanceLock = new ReentrantLock();
    private static Lock connectionLock = new ReentrantLock();
    private static AtomicBoolean init;
    private Deque<Connection> listConnections;
    private Semaphore semaphore;
    private ConnectionFactory connectionFactory;

    static {
        init = new AtomicBoolean();
    }

    private ConnectionPool() {
        listConnections = new ArrayDeque<>();
        semaphore = new Semaphore(CONNECTIONS_AMOUNT, true);
    }

    public static ConnectionPool getInstance() {
        if (!init.get()) {
            try {
                instanceLock.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                    instance.init();
                    init.set(true);
                }
            } finally {
                instanceLock.unlock();
            }
        }
        return instance;
    }

    private void init() {
        for (int i = 0; i < CONNECTIONS_AMOUNT; i++) {
            connectionFactory = new ConnectionFactory();
            listConnections.add(connectionFactory.createConnection());
        }
    }

    public Connection getConnection() throws ConnectionPoolException{
        try {
            connectionLock.lock();
            semaphore.acquire();
            return listConnections.poll();
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e.getMessage(), e);
        } finally {
            connectionLock.unlock();
        }
    }

    public void returnConnection(Connection connection) {
        try {
            connectionLock.lock();
            semaphore.release();
            listConnections.add(connection);
        } finally {
            connectionLock.unlock();
        }
    }

    public void closeConnections() {
        try {
            connectionLock.lock();
            for (Connection connection : listConnections) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        logger.warn(e.getMessage(), e);
                    }
                }
            }
        } finally {
            connectionLock.unlock();
        }

    }
}
