package com.epam.totalizator.jdbc;


import com.epam.totalizator.exceptions.PropertiesReadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {
    private final static Logger logger = LogManager.getLogger(ConnectionFactory.class);

    private static final String PROPERTIES_FILE = "database_properties.properties";
    private static final String URL_PROPERTY = "url";

    private static Properties properties;

    public Connection createConnection(){
        Connection connection = null;
        try {
            readProperties();
            String databaseUrl = properties.getProperty(URL_PROPERTY);
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            connection = DriverManager.getConnection(databaseUrl, properties);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        } catch (PropertiesReadException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
        return connection;
    }

    private void readProperties() throws PropertiesReadException{
        if (properties == null) {
            properties = new Properties();
            try {
                properties.load(this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE));
            } catch (IOException e) {
                throw new PropertiesReadException(e.getMessage(), e);
            }
        }
    }
}
