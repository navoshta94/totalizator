package com.epam.totalizator.entity;

import java.math.BigDecimal;

/**
 * Created by e.navosha on 17.01.19.
 */
public class BetEvent implements Identifable{
    public static final String ID = "id";
    public static final String COMPETITION_ID = "fk_competition_id";
    public static final String EVENT_TYPE = "event_type";
    public static final String COEFFICIENT = "coefficient";

    private Long id;
    private Long competitionID;
    private EventType eventType;
    private BigDecimal coefficient;

    private BetEvent(Long competitionID, EventType eventType, BigDecimal coefficient) {
        this.competitionID = competitionID;
        this.eventType = eventType;
        this.coefficient = coefficient;
    }

    public static BetEvent firsCompetitorWin(Long competitionID, BigDecimal coefficient) {
        return new BetEvent(competitionID, EventType.WIN_FIRST_COMPETITOR, coefficient);
    }

    public static BetEvent secondCompetitorWin(Long competitionID, BigDecimal coefficient) {
        return new BetEvent(competitionID, EventType.WIN_SECOND_COMPETITOR, coefficient);
    }

    public static BetEvent draw(Long competitionID, BigDecimal coefficient) {
        return new BetEvent(competitionID, EventType.DRAW, coefficient);
    }

    public static BetEvent exactScore(Long competitionID, BigDecimal coefficient) {
        return new BetEvent(competitionID, EventType.EXACT_SCORE, coefficient);
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompetitionID() {
        return competitionID;
    }

    public void setCompetitionID(Long competitionID) {
        this.competitionID = competitionID;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public BigDecimal getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(BigDecimal coefficient) {
        this.coefficient = coefficient;
    }
}
