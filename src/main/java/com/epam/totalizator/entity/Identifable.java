package com.epam.totalizator.entity;

public interface Identifable {

    Long getId();
}
