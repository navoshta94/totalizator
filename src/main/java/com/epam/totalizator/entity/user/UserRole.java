package com.epam.totalizator.entity.user;

public enum UserRole {
    CLIENT, ADMIN, BOOKMAKER
}
