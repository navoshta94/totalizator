package com.epam.totalizator.entity.user;

import com.epam.totalizator.entity.Identifable;

import java.math.BigDecimal;

public class User implements Identifable {
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";
    public static final String ID = "id";
    public static final String CASH_AMOUNT = "cash_amount";
    public static final String IS_ACTIVE = "is_active";

    private Long id;
    private Boolean isActive;
    private String login;
    private String password;
    private UserRole role;
    private BigDecimal cashAmount;

    public User(Long id, String login, String password, UserRole role, BigDecimal cashAmount, Boolean isActive) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
        this.cashAmount = cashAmount;
        this.isActive = isActive;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public BigDecimal getCashAmmount() {
        return cashAmount;
    }

    public void setCashAmmount(BigDecimal cashAmmount) {
        this.cashAmount = cashAmmount;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
