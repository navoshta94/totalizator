package com.epam.totalizator.entity.bet;

public enum BetKind {
    EXACT_RESULT, EXACT_SCORE
}
