package com.epam.totalizator.entity.bet;

import com.epam.totalizator.entity.Identifable;

import java.math.BigDecimal;

public class Bet implements Identifable {
    public static final String ID = "id";
    public static final String COMPETITION_ID = "fk_competition_id";
    public static final String USER_ID = "fk_user_id";
    public static final String FIRST_WIN = "first_win";
    public static final String DRAW = "draw";
    public static final String SECOND_WIN = "second_win";
    public static final String FIRST_COMPETITOR_SCORE = "first_competitor_score";
    public static final String SECOND_COMPETITOR_SCORE = "second_competitor_score";
    public static final String BET_KIND = "bet_kind";
    public static final String BET_AMOUNT = "bet_amount";
    public static final String WIN_BET_AMOUNT = "win_bet_amount";
    public static final String IS_WIN = "is_win";

    private Long id;
    private Long competitionID;
    private Long userID;
    private Boolean firstWin;
    private Boolean draw;
    private Boolean secondWin;
    private Integer firstCompetitorScore;
    private Integer secondCompetitorScore;
    private BetKind betKind;
    private BigDecimal betAmount;
    private BigDecimal winBetAmount;
    private Boolean isWin;

    public Bet(Long id, Long competitionID, Long userID, Boolean firstWin, Boolean draw, Boolean secondWin,
               Integer firstCompetitorScore, Integer secondCompetitorScore, BetKind betKind,
               BigDecimal betAmount, BigDecimal winBetAmount, Boolean isWin) {
        this.id = id;
        this.competitionID = competitionID;
        this.userID = userID;
        this.firstWin = firstWin;
        this.draw = draw;
        this.secondWin = secondWin;
        this.firstCompetitorScore = firstCompetitorScore;
        this.secondCompetitorScore = secondCompetitorScore;
        this.betKind = betKind;
        this.betAmount = betAmount;
        this.winBetAmount = winBetAmount;
        this.isWin = isWin;
    }

    public Bet(Long competitionID, Long userID, BetKind betKind,
               Boolean firstWin, Boolean draw, Boolean secondWin,
               BigDecimal betAmount, BigDecimal winBetAmount) {
        this.competitionID = competitionID;
        this.userID = userID;
        this.firstWin = firstWin;
        this.draw = draw;
        this.secondWin = secondWin;
        this.betKind = betKind;
        this.betAmount = betAmount;
        this.winBetAmount = winBetAmount;
    }

    public Bet(Long competitionID, Long userID, BetKind betKind,
               Integer firstCompetitorScore, Integer secondCompetitorScore,
               BigDecimal betAmount, BigDecimal winBetAmount) {
        this.competitionID = competitionID;
        this.userID = userID;
        this.firstCompetitorScore = firstCompetitorScore;
        this.secondCompetitorScore = secondCompetitorScore;
        this.betKind = betKind;
        this.betAmount = betAmount;
        this.winBetAmount = winBetAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompetitionID() {
        return competitionID;
    }

    public void setCompetitionID(Long competitionID) {
        this.competitionID = competitionID;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Boolean getFirstWin() {
        return firstWin;
    }

    public void setFirstWin(Boolean firstWin) {
        this.firstWin = firstWin;
    }

    public Boolean getDraw() {
        return draw;
    }

    public void setDraw(Boolean draw) {
        this.draw = draw;
    }

    public Boolean getSecondWin() {
        return secondWin;
    }

    public void setSecondWin(Boolean secondWin) {
        this.secondWin = secondWin;
    }

    public Integer getFirstCompetitorScore() {
        return firstCompetitorScore;
    }

    public void setFirstCompetitorScore(Integer firstCompetitorScore) {
        this.firstCompetitorScore = firstCompetitorScore;
    }

    public Integer getSecondCompetitorScore() {
        return secondCompetitorScore;
    }

    public void setSecondCompetitorScore(Integer secondCompetitorScore) {
        this.secondCompetitorScore = secondCompetitorScore;
    }

    public BetKind getBetKind() {
        return betKind;
    }

    public void setBetKind(BetKind betKind) {
        this.betKind = betKind;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getWinBetAmount() {
        return winBetAmount;
    }

    public void setWinBetAmount(BigDecimal winBetAmount) {
        this.winBetAmount = winBetAmount;
    }

    public Boolean getWin() {
        return isWin;
    }

    public void setWin(Boolean win) {
        isWin = win;
    }
}
