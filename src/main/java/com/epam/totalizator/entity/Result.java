package com.epam.totalizator.entity;


public class Result implements Identifable {
    public static final String ID = "id";
    public static final String COMPETITION_ID = "fk_competition_id";
    public static final String FIRST_COMPETITOR_SCORE = "first_competitor_score";
    public static final String SECOND_COMPETITOR_SCORE = "second_competitor_score";

    private Long id;
    private Long competitionID;
    private Integer firstCompetitorScore;
    private Integer secondCompetitorScore;

    public Result(Long competitionID, Integer firstCompetitorScore, Integer secondCompetitorScore) {
        this.competitionID = competitionID;
        this.firstCompetitorScore = firstCompetitorScore;
        this.secondCompetitorScore = secondCompetitorScore;
    }

    public Result(Long id, Long competitionID, Integer firstCompetitorScore, Integer secondCompetitorScore) {
        this.id = id;
        this.competitionID = competitionID;
        this.firstCompetitorScore = firstCompetitorScore;
        this.secondCompetitorScore = secondCompetitorScore;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompetitionID() {
        return competitionID;
    }

    public void setCompetitionID(Long competitionID) {
        this.competitionID = competitionID;
    }

    public Integer getFirstCompetitorScore() {
        return firstCompetitorScore;
    }

    public void setFirstCompetitorScore(Integer firstCompetitorScore) {
        this.firstCompetitorScore = firstCompetitorScore;
    }

    public Integer getSecondCompetitorScore() {
        return secondCompetitorScore;
    }

    public void setSecondCompetitorScore(Integer secondCompetitorScore) {
        this.secondCompetitorScore = secondCompetitorScore;
    }
}
