package com.epam.totalizator.entity;

public enum EventType {
    WIN_FIRST_COMPETITOR, DRAW, WIN_SECOND_COMPETITOR, EXACT_SCORE
}
