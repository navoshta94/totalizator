package com.epam.totalizator.entity;

import java.time.LocalDateTime;

public class Competition implements Identifable{
    public static final String ID = "id";
    public static final String FIRST_COMPETITOR = "first_competitor";
    public static final String SECOND_COMPETITOR = "second_competitor";
    public static final String COMPETITION_BEGINNING = "competition_beginning";
    public static final String START_DATE = "startDate";
    public static final String START_TIME = "startTime";

    private Long id;
    private String firstCompetitor;
    private String secondCompetitor;
    private LocalDateTime beginning;

    public Competition(LocalDateTime beginning, String firstCompetitor, String secondCompetitor) {
        this.beginning = beginning;
        this.firstCompetitor = firstCompetitor;
        this.secondCompetitor = secondCompetitor;
    }

    public Competition(Long id, String firstCompetitor, String secondCompetitor, LocalDateTime beginning) {
        this.id = id;
        this.firstCompetitor = firstCompetitor;
        this.secondCompetitor = secondCompetitor;
        this.beginning = beginning;
    }

    public String getFirstCompetitor() {
        return firstCompetitor;
    }

    public void setFirstCompetitor(String firstCompetitor) {
        this.firstCompetitor = firstCompetitor;
    }

    public String getSecondCompetitor() {
        return secondCompetitor;
    }

    public void setSecondCompetitor(String secondCompetitor) {
        this.secondCompetitor = secondCompetitor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getBeginning() {
        return beginning;
    }

    public void setBeginning(LocalDateTime beginning) {
        this.beginning = beginning;
    }
}
