package com.epam.totalizator.servlet;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.jdbc.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Servlet extends HttpServlet {
    final static Logger logger = LogManager.getLogger(Servlet.class);

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
            process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) {
        String command = req.getParameter("command");
        Command action = CommandFactory.create(command);
        logger.info(command);
        CommandResult result = null;
        try {
            result = action.execute(req, resp);
            dispatch(req, resp, result);
        } catch (Exception e) {

            logger.warn(e.getMessage(), e);
        }
    }

    private void dispatch(HttpServletRequest req, HttpServletResponse resp, CommandResult result) throws ServletException, IOException {
        if (result.isRedirect()) {
            resp.sendRedirect(result.getPage());
        } else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(result.getPage());
            dispatcher.forward(req, resp);
        }
    }

}
