package com.epam.totalizator.listener;

import com.epam.totalizator.jdbc.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class ContextListenerImp implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(ContextListenerImp.class);
    private static final String CONNECTION_POOL = "connectionPool";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        servletContext.setAttribute(CONNECTION_POOL, connectionPool);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        ConnectionPool connectionPool = (ConnectionPool) servletContext.getAttribute(CONNECTION_POOL);
        connectionPool.closeConnections();

    }
}

