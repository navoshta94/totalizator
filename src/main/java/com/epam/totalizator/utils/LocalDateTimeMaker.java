package com.epam.totalizator.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeMaker {

    private static final String DATE_FORMAT = "dd.MM.yyyy HH:mm";

    public LocalDateTime makeLocalDateTime(String date, String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        String dateTime = date + " " + time;
        return LocalDateTime.parse(dateTime, formatter);
    }
}
