package com.epam.totalizator.utils;

import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.EventType;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.entity.bet.BetKind;

import java.math.BigDecimal;

public class BetCreator {

    public Bet createBet(BetEvent betEvent, Long competitionID, Long userID,
                      Integer firstCompetitorScore, Integer secondCompetitorScore,
                      BigDecimal betAmount) {
        EventType eventType = betEvent.getEventType();
        switch (eventType) {
            case WIN_FIRST_COMPETITOR:
                return createFirstCompetitorWinBet(competitionID, userID,
                        betEvent.getCoefficient(), betAmount);
            case DRAW:
                return createDrawBet(competitionID, userID,
                        betEvent.getCoefficient(), betAmount);
            case WIN_SECOND_COMPETITOR:
                return createSecondCompetitorWinBet(competitionID, userID,
                        betEvent.getCoefficient(), betAmount);
            case EXACT_SCORE:
                return createExactScoreBet(competitionID, userID,
                        betEvent.getCoefficient(), betAmount,
                        firstCompetitorScore, secondCompetitorScore);
                default:
                    throw new IllegalStateException();
        }
    }
    private Bet createFirstCompetitorWinBet(Long competitionID, Long userID,
                                            BigDecimal coefficient, BigDecimal betAmount) {
        BigDecimal winBetAmount = calculateWinAmount(coefficient, betAmount);
        return new Bet(competitionID, userID, BetKind.EXACT_RESULT,
                 true, false, false,
                 betAmount, winBetAmount);
    }

    private Bet createDrawBet(Long competitionID, Long userID,
                              BigDecimal coefficient, BigDecimal betAmount) {
        BigDecimal winBetAmount = calculateWinAmount(coefficient, betAmount);
        return new Bet(competitionID, userID, BetKind.EXACT_RESULT,
                false, true, false,
                betAmount, winBetAmount);
    }

    private Bet createSecondCompetitorWinBet(Long competitionID, Long userID,
                                             BigDecimal coefficient, BigDecimal betAmount) {
        BigDecimal winBetAmount = calculateWinAmount(coefficient, betAmount);
        return new Bet(competitionID, userID, BetKind.EXACT_RESULT,
                false, false, true,
                betAmount, winBetAmount);
    }

    private Bet createExactScoreBet(Long competitionID, Long userID,
                                    BigDecimal coefficient, BigDecimal betAmount,
                                    Integer firstCompetitorScore, Integer secondCompetitorScore) {
        BigDecimal winBetAmount = calculateWinAmount(coefficient, betAmount);
        return new Bet(competitionID, userID, BetKind.EXACT_SCORE, firstCompetitorScore,
                secondCompetitorScore, betAmount, winBetAmount);
    }

    private BigDecimal calculateWinAmount(BigDecimal coefficient, BigDecimal betAmount) {
        return betAmount.multiply(coefficient);
    }

}
