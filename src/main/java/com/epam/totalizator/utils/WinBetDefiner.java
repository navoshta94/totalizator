package com.epam.totalizator.utils;

import com.epam.totalizator.entity.Result;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.entity.bet.BetKind;

/**
 * Created by e.navosha on 23.01.19.
 */
public class WinBetDefiner {

    public boolean isWinBet(Result result, Bet bet) {
        int firstCompetitorScore = result.getFirstCompetitorScore();
        int secondCompetitorScore = result.getSecondCompetitorScore();
        boolean firstCompetitorWin = firstCompetitorScore > secondCompetitorScore;
        boolean draw = firstCompetitorScore == secondCompetitorScore;
        boolean secondCompetitorWin = firstCompetitorScore < secondCompetitorScore;
        if (bet.getBetKind() == BetKind.EXACT_SCORE) {
            if (firstCompetitorScore == (int)bet.getFirstCompetitorScore() &&
                    secondCompetitorScore == (int)bet.getSecondCompetitorScore()) {
                return true;
            }
        } else {
            if ((firstCompetitorWin == bet.getFirstWin()) &&
                    (draw == bet.getDraw()) &&
                    (secondCompetitorWin == secondCompetitorWin)) {
                return true;
            }
        }
        return false;
    }
}
