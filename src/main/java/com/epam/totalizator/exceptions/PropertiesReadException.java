package com.epam.totalizator.exceptions;

/**
 * Created by e.navosha on 06.12.18.
 */
public class PropertiesReadException extends Exception{

    public PropertiesReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
