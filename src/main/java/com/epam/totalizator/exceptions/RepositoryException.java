package com.epam.totalizator.exceptions;

public class RepositoryException extends Exception {

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
