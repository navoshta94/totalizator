package com.epam.totalizator.exceptions;

public class ConnectionPoolException extends Exception{

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
