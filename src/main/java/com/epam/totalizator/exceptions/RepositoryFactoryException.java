package com.epam.totalizator.exceptions;

public class RepositoryFactoryException extends Exception{

    public RepositoryFactoryException(String message, Throwable cause) {
        super(message, cause);
    }
    public RepositoryFactoryException(Throwable cause) {
        super(cause);
    }
}
