package com.epam.totalizator.service;

import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.EventType;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.repository.Repository;
import com.epam.totalizator.repository.RepositoryFactory;
import com.epam.totalizator.repository.specification.BetEventByTypeSpecification;
import com.epam.totalizator.repository.specification.CoefficientsByCompetitionIdSpecification;
import com.epam.totalizator.repository.specification.Specification;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class BetEventService {

    public Optional<BetEvent> findBetEventByType(EventType eventType) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<BetEvent> repository = repositoryFactory.getCoefficientRepository();
            Specification specification = new BetEventByTypeSpecification(eventType);
            return repository.querySingleResult(specification);
        } catch (RepositoryException | RepositoryFactoryException e) {
            throw new ServiceException(e);
        }
    }

    public List<BetEvent> findCoefficientsByCompetitionID(Long competitionID) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<BetEvent> repository = repositoryFactory.getCoefficientRepository();
            Specification specification = new CoefficientsByCompetitionIdSpecification(competitionID);
            return repository.query(specification);
        } catch (RepositoryException | RepositoryFactoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void setUpCoefficients(Long competitionID,
                                  BigDecimal firsCompetitorWinCoef,
                                  BigDecimal drawCoef,
                                  BigDecimal secondCompetitorWinCoef,
                                  BigDecimal exactResultCoef
                                 ) throws ServiceException {

        try (RepositoryFactory repositoryFactory = new RepositoryFactory()){
            Repository<BetEvent> repository = repositoryFactory.getCoefficientRepository();
            BetEvent firstCompetitorWin = BetEvent.firsCompetitorWin(competitionID, firsCompetitorWinCoef);
            BetEvent draw = BetEvent.draw(competitionID, drawCoef);
            BetEvent secondCompetitorWin = BetEvent.secondCompetitorWin(competitionID, secondCompetitorWinCoef);
            BetEvent exactScore = BetEvent.exactScore(competitionID, exactResultCoef);
            List<BetEvent> betEvents = Arrays.asList(firstCompetitorWin, draw, secondCompetitorWin, exactScore);
            for (BetEvent betEvent : betEvents) {
                repository.add(betEvent);
            }
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }
}
