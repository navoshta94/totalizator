package com.epam.totalizator.service;

import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.Result;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.repository.Repository;
import com.epam.totalizator.repository.RepositoryFactory;
import com.epam.totalizator.repository.specification.FindAllSpecification;
import com.epam.totalizator.repository.specification.FindByIdSpecification;
import com.epam.totalizator.repository.specification.FindResultByCompetitionID;
import com.epam.totalizator.repository.specification.Specification;
import com.epam.totalizator.utils.BetCreator;
import com.epam.totalizator.utils.WinBetDefiner;

import java.math.BigDecimal;
import java.util.List;

public class BetService {

    public void addBet(Long competitionID, Long userID, BetEvent betEvent,
                       Integer firstCompetitorScore, Integer secondCompetitorScore,
                       BigDecimal betAmount) throws ServiceException {

        BetCreator betCreator = new BetCreator();
        Bet bet = betCreator.createBet(betEvent, competitionID, userID,
                firstCompetitorScore, secondCompetitorScore, betAmount);
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()){
            Repository<Bet> repository = repositoryFactory.getBetRepository();
            repository.add(bet);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public List<Bet> findBetsByCompetitionID(Long competitionID) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Bet> repository = repositoryFactory.getBetRepository();
            Specification specification = new FindResultByCompetitionID(competitionID);
            return repository.query(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public void saveBetResult(Result result, Long competitionID) throws ServiceException {
        WinBetDefiner winBetDefiner = new WinBetDefiner();
        BetService betService = new BetService();
        List<Bet> betList = betService.findBetsByCompetitionID(competitionID);
        betList.forEach(bet -> {
            if (winBetDefiner.isWinBet(result, bet)) {
                bet.setWin(true);
            } else {
                bet.setWin(false);
            }
        });

        try (RepositoryFactory repositoryFactory = new RepositoryFactory()){
            Repository<Bet> betRepository = repositoryFactory.getBetRepository();
            for (Bet bet : betList) {
                betRepository.save(bet);
            }
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }
}
