package com.epam.totalizator.service;

import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.repository.Repository;
import com.epam.totalizator.repository.RepositoryFactory;
import com.epam.totalizator.repository.specification.FindAllSpecification;
import com.epam.totalizator.repository.specification.FindByIdSpecification;
import com.epam.totalizator.repository.specification.Specification;
import com.epam.totalizator.utils.LocalDateTimeMaker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

public class CompetitionService {


    public List<Competition> acquireCompetitionList() throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Competition> repository = repositoryFactory.getCompetitionRepository();
            Specification specification = new FindAllSpecification();
            return repository.query(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public void createCompetition(String firstCompetitor, String secondCompetitor,
                                  LocalDateTime beginning) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {

            Repository<Competition> repository = repositoryFactory.getCompetitionRepository();
            Competition competition = new Competition(beginning, firstCompetitor, secondCompetitor);
            repository.add(competition);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }

    }


    public Optional<Competition> findCompetitionById(Long id) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Competition> repository = repositoryFactory.getCompetitionRepository();
            Specification specification = new FindByIdSpecification(id);
            return repository.querySingleResult(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }

    }

    public void editCompetition(Long id, String firstCompetitor, String secondCompetitor,
                                LocalDateTime beginning) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Competition> repository = repositoryFactory.getCompetitionRepository();
            repository.save(new Competition(id, firstCompetitor, secondCompetitor, beginning));
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

}
