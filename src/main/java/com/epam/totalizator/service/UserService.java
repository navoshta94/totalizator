package com.epam.totalizator.service;

import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.repository.Repository;
import com.epam.totalizator.repository.RepositoryFactory;
import com.epam.totalizator.repository.specification.FindAllSpecification;
import com.epam.totalizator.repository.specification.FindByIdSpecification;
import com.epam.totalizator.repository.specification.Specification;
import com.epam.totalizator.repository.specification.UserByLoginAndPasswordSpecification;

import java.util.List;
import java.util.Optional;

public class UserService {

    public Optional<User> login(String login, String password) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {

            Repository<User> userRepository = repositoryFactory.getUserRepository();
            Specification specification = new UserByLoginAndPasswordSpecification(login, password);
            return userRepository.querySingleResult(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<User> findUserByID(Long id) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {

            Repository<User> userRepository = repositoryFactory.getUserRepository();
            Specification specification = new FindByIdSpecification(id);
            return userRepository.querySingleResult(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public List<User> acquireUserList() throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            Specification specification = new FindAllSpecification();
            return repository.query(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public void changeUserStatus(User user, Boolean isUserActive) throws ServiceException {
        user.setIsActive(isUserActive);
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> userRepository = repositoryFactory.getUserRepository();
            userRepository.save(user);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }
}
