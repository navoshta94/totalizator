package com.epam.totalizator.service;

import com.epam.totalizator.entity.Result;
import com.epam.totalizator.exceptions.RepositoryException;
import com.epam.totalizator.exceptions.RepositoryFactoryException;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.generator.ResultGenerator;
import com.epam.totalizator.repository.Repository;
import com.epam.totalizator.repository.RepositoryFactory;
import com.epam.totalizator.repository.specification.FindByIdSpecification;
import com.epam.totalizator.repository.specification.FindResultByCompetitionID;
import com.epam.totalizator.repository.specification.Specification;

import java.util.Optional;

public class ResultService {
    public Optional<Result> findResultByCompetitionID(Long competitionID) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Result> resultRepository = repositoryFactory.getResultRepository();
            Specification specification = new FindResultByCompetitionID(competitionID);
            return resultRepository.querySingleResult(specification);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    public void addResult(Long competitionID) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Result> resultRepository = repositoryFactory.getResultRepository();
            ResultGenerator resultGenerator = new ResultGenerator();
            Result result = resultGenerator.generate(competitionID);
            resultRepository.add(result);
        } catch (RepositoryFactoryException | RepositoryException e) {
            throw new ServiceException(e);
        }
    }
}
