package com.epam.totalizator.comand.bookmaker;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.BetEventService;
import com.epam.totalizator.service.CompetitionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class SetUpCoefficientPageCommand implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        Long id = Long.parseLong(request.getParameter("competitionID"));
        BetEventService betEventService = new BetEventService();
        List<BetEvent> events = betEventService.findCoefficientsByCompetitionID(id);
        if (events.isEmpty()) {
            CompetitionService competitionService = new CompetitionService();
            Optional<Competition> competition = competitionService.findCompetitionById(id);
            if (competition.isPresent()) {
                request.setAttribute("competition", competition.get());
            }
            return CommandResult.forward("/WEB-INF/pages/set_coef.jsp");
        } else
            return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE +"&message=" + "betEvent.setted");
    }
}
