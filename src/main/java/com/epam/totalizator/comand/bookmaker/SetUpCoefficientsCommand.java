package com.epam.totalizator.comand.bookmaker;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.BetEventService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

public class SetUpCoefficientsCommand implements Command {
    private static final String COMPETITION_ID_PARAM = "competitionID";
    private static final String FIRST_COMPETITOR_WIN_PARAM = "winFirst";
    private static final String DRAW_PARAM = "draw";
    private static final String SECOND_COMPETITOR_WIN_PARAM = "winSecond";
    private static final String EXACT_RESULT_PARAM = "exactScore";


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        BetEventService betEventService = new BetEventService();
        Long competitionID = Long.parseLong(request.getParameter(COMPETITION_ID_PARAM));
        BigDecimal firstCompetitorWin = new BigDecimal(request.getParameter(FIRST_COMPETITOR_WIN_PARAM));
        BigDecimal draw = new BigDecimal(request.getParameter(DRAW_PARAM));
        BigDecimal secondCompetitorWin = new BigDecimal(request.getParameter(SECOND_COMPETITOR_WIN_PARAM));
        BigDecimal exactResult = new BigDecimal(request.getParameter(EXACT_RESULT_PARAM));
        betEventService.setUpCoefficients(competitionID, firstCompetitorWin, draw, secondCompetitorWin, exactResult);
        return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE);
    }
}
