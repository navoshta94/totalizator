package com.epam.totalizator.comand.client;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.EventType;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.BetEventService;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Optional;

public class MakeBetCommand implements Command {
    private static final String EVENT_TYPE = "eventType";
    private static final String BET_SUCCESS = "bet.maded";
    private static final String BET_FAILURE = "?command=makeBetPage&message=makebet.error";
    private static final String COMPETITION_ID = "competitionID";
    private static final String FIRST_COMPETITOR_SCORE = "firstCompetitorScore";
    private static final String SECOND_COMPETITOR_SCORE = "secondCompetitorScore";
    private static final String BET_AMOUNT = "betAmount";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String message = request.getParameter("message");
        EventType eventType = EventType.valueOf(request.getParameter(EVENT_TYPE).toUpperCase());
        Long competitionID = Long.parseLong(request.getParameter(COMPETITION_ID));
        Integer firstCompetitorScore = null;
        Integer secondCompetitorScore = null;
        if (eventType == EventType.EXACT_SCORE) {
            firstCompetitorScore = Integer.parseInt(request.getParameter(FIRST_COMPETITOR_SCORE));
            secondCompetitorScore = Integer.parseInt(request.getParameter(SECOND_COMPETITOR_SCORE));
        }
        BigDecimal betAmount = new BigDecimal(request.getParameter(BET_AMOUNT));
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        Long userID = user.getId();
        Optional<BetEvent> betEvent = findBetEvent(eventType);
        if (betEvent.isPresent()) {
            addBetEvent(competitionID, userID, betEvent.get(), firstCompetitorScore, secondCompetitorScore, betAmount);
            return CommandResult.redirect("?command="+ CommandFactory.MAKE_BET_PAGE + "&competitionID="+competitionID+
                    "&message="+ BET_SUCCESS);
        }
        return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE + "&message=error");
    }

    private Optional<BetEvent> findBetEvent(EventType eventType) throws ServiceException {
        BetEventService betEventService = new BetEventService();
        return betEventService.findBetEventByType(eventType);
    }

    private void addBetEvent(Long competitionID, Long userID,
                             BetEvent betEvent, Integer firstCompetitorScore,
                             Integer secondCompetitorScore, BigDecimal betAmount) throws ServiceException {
        BetService betService = new BetService();
        betService.addBet(competitionID, userID, betEvent, firstCompetitorScore, secondCompetitorScore,
                betAmount);
    }
}
