package com.epam.totalizator.comand.client;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.BetEvent;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.BetEventService;
import com.epam.totalizator.service.CompetitionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class MakeBetPageCommand implements Command {
    private final static String COMPETITION_ID = "competitionID";
    private final static String MAKE_BET_PAGE = "/WEB-INF/pages/make_bet.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        Long competitionID = Long.parseLong(request.getParameter(COMPETITION_ID));

        CompetitionService competitionService = new CompetitionService();
        Optional<Competition> competition = competitionService.findCompetitionById(competitionID);
        request.setAttribute("competition", competition.get());

        BetEventService betEventService = new BetEventService();
        List<BetEvent> betEvents = betEventService.findCoefficientsByCompetitionID(competitionID);
        if (betEvents.isEmpty()) {
            return CommandResult.redirect("?command="+ CommandFactory.MAIN_PAGE + "&message=error.betEvent.empty");
        }
        request.setAttribute("betEvents", betEvents);
        String message = request.getParameter("message");
        request.setAttribute("message", message);
        return CommandResult.forward(MAKE_BET_PAGE);
    }
}
