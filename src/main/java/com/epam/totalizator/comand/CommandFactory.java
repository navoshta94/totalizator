package com.epam.totalizator.comand;

import com.epam.totalizator.comand.admin.*;
import com.epam.totalizator.comand.bookmaker.SetUpCoefficientPageCommand;
import com.epam.totalizator.comand.bookmaker.SetUpCoefficientsCommand;
import com.epam.totalizator.comand.client.MakeBetCommand;
import com.epam.totalizator.comand.client.MakeBetPageCommand;
import com.epam.totalizator.comand.common.LoginCommand;
import com.epam.totalizator.comand.common.LoginPageCommand;
import com.epam.totalizator.comand.common.MainPageCommand;
import com.epam.totalizator.comand.common.SignOutCommand;

public class CommandFactory {
    public static final String LOGIN = "login";
    public static final String SIGN_OUT = "signOut";
    public static final String MAIN_PAGE = "mainPage";
    public static final String CREATE_COMPETITION_PAGE = "competitionCreatingPage";
    public static final String CREATE_COMPETITION = "competitionCreating";
    public static final String EDIT_COMPETITION_PAGE = "competitionEditingPage";
    public static final String EDIT_COMPETITION = "editCompetition";
    public static final String MAKE_BET_PAGE = "makeBetPage";
    public static final String MAKE_BET = "makeBet";
    public static final String SET_UP_COEFFICIENTS_PAGE = "setUpCoefPage";
    public static final String SET_UP_COEFFICIENTS = "setUpCoefficients";
    public static final String USER_MANAGEMENT_PAGE= "userManagement";
    public static final String CHANGE_USER_STATUS = "changeUserStatus";
    public static final String LOGIN_PAGE = "loginPage";
    public static final String SET_RESULT = "setCompetitionResult";
    public static final String SET_BET_RESULT = "setBetResult";

    public static Command create(String name) {
        switch (name) {
            case LOGIN:
                return new LoginCommand();
            case MAIN_PAGE:
                return new MainPageCommand();
            case CREATE_COMPETITION_PAGE:
                return new CompetitionCreatingPageCommand();
            case CREATE_COMPETITION:
                return new CompetitionCreatingCommand();
            case EDIT_COMPETITION_PAGE:
                return new CompetitionEditingPageCommand();
            case MAKE_BET_PAGE:
                return new MakeBetPageCommand();
            case MAKE_BET:
                return new MakeBetCommand();
            case EDIT_COMPETITION:
                return new CompetitionEditingCommand();
            case SIGN_OUT:
                return new SignOutCommand();
            case SET_UP_COEFFICIENTS_PAGE:
                return new SetUpCoefficientPageCommand();
            case SET_UP_COEFFICIENTS:
                return new SetUpCoefficientsCommand();
            case USER_MANAGEMENT_PAGE:
                return new UserManagementPageCommand();
            case CHANGE_USER_STATUS:
                return new ChangeUserStatusComand();
            case LOGIN_PAGE:
                return new LoginPageCommand();
            case SET_RESULT:
                return new SetCompetitionResultCommand();
            case SET_BET_RESULT:
                return new SetBetResultCommand();
            default:
                throw new IllegalStateException();
        }
    }
}
