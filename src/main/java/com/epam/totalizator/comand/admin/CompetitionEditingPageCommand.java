package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.CompetitionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class CompetitionEditingPageCommand implements Command {
    private static final String EDIT_COMPETITION_PAGE = "/WEB-INF/pages/edit_competition.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CompetitionService service = new CompetitionService();
        Long id = Long.parseLong(request.getParameter("competitionID"));
        Optional<Competition> competition = service.findCompetitionById(id);
        request.setAttribute("competition", competition.get());
        return CommandResult.forward(EDIT_COMPETITION_PAGE);
    }
}
