package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CompetitionCreatingPageCommand implements Command {
    private static final String CREATE_COMPETITION_PAGE = "/WEB-INF/pages/create_competition.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        return CommandResult.forward(CREATE_COMPETITION_PAGE);
    }
}
