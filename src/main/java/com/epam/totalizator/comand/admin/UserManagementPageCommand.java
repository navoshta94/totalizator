package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserManagementPageCommand implements Command {
    private static final String USER_MANAGEMENT_PAGE = "/WEB-INF/pages/user_management.jsp";
    private static final String USERS = "users";
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        UserService userService = new UserService();
        List<User> userList = userService.acquireUserList();
        request.setAttribute(USERS, userList);
        return CommandResult.forward(USER_MANAGEMENT_PAGE);
    }
}
