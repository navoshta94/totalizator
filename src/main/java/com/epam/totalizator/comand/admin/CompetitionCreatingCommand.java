package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.CompetitionService;
import com.epam.totalizator.utils.LocalDateTimeMaker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class CompetitionCreatingCommand implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        CompetitionService service = new CompetitionService();
        String firstCompetitor = request.getParameter(Competition.FIRST_COMPETITOR);
        String secondCompetitor = request.getParameter(Competition.SECOND_COMPETITOR);
        LocalDateTimeMaker localDateTimeMaker = new LocalDateTimeMaker();
        LocalDateTime beginning = localDateTimeMaker.makeLocalDateTime(request.getParameter(Competition.START_DATE),
                request.getParameter(Competition.START_TIME));
        service.createCompetition(firstCompetitor, secondCompetitor, beginning);
        return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE);
    }
}
