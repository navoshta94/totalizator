package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ChangeUserStatusComand implements Command {
    private static final String USER_MANAGEMENT = "?command=userManagement";
    private static final String USER_ID = "userID";
    private static final String IS_ACTIVATE = "isActivate";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        Long userID = Long.parseLong(request.getParameter(USER_ID));
        Boolean isActivateUser = Boolean.parseBoolean(request.getParameter(IS_ACTIVATE));
        UserService userService = new UserService();
        Optional<User> user = userService.findUserByID(userID);
        if (user.isPresent()) {
            userService.changeUserStatus(user.get(), isActivateUser);
        } else {
            request.setAttribute("error", "error");
        }
        return CommandResult.redirect("?command=" + CommandFactory.USER_MANAGEMENT_PAGE);
    }

}
