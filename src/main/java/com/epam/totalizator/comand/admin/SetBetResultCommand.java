package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.Result;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.BetService;
import com.epam.totalizator.service.ResultService;
import com.epam.totalizator.utils.WinBetDefiner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SetBetResultCommand implements Command{
    private static final String MAIN_PAGE = "/WEB-INF/pages/main.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        Long competitionID = Long.parseLong(request.getParameter("competitionID"));
        ResultService resultService = new ResultService();
        Optional<Result> result = resultService.findResultByCompetitionID(competitionID);
        if (result.isPresent()) {
            BetService betService = new BetService();
            betService.saveBetResult(result.get(), competitionID);
            return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE + "&message=result.setted");
        }
        return CommandResult.redirect("?command=" + CommandFactory.MAIN_PAGE);
    }
}
