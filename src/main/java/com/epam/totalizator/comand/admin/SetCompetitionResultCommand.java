package com.epam.totalizator.comand.admin;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandFactory;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.Result;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.ResultService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class SetCompetitionResultCommand implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        Long competitionID = Long.parseLong(request.getParameter("competitionID"));
        ResultService resultService = new ResultService();
        Optional<Result> result = resultService.findResultByCompetitionID(competitionID);
        if (result.isPresent()) {
            request.setAttribute("message", "Result have been setted");
            return CommandResult.redirect("?command="+CommandFactory.MAIN_PAGE +"&message=error.result.setted");
        }
        resultService.addResult(competitionID);
        return CommandResult.redirect("?command="+ CommandFactory.SET_BET_RESULT+"&competitionID="+competitionID);
    }
}
