package com.epam.totalizator.comand.common;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.user.User;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class LoginCommand implements Command {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USER_ATTR = "user";
    private static final String ERROR_MESSAGE_ATTR = "errorMessage";
    private static final String AUTHORIZATION_ERROR_MESSAGE = "login.error.authorization";
    private static final String USER_BLOCKED_MESSAGE = "login.error.blocked";
    private static final String LOGIN_PAGE = "/WEB-INF/pages/login.jsp";
    private static final String MAIN_PAGE_COMMAND = "?command=mainPage";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        UserService service = new UserService();
        String name = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        Optional<User> optUser = null;
        optUser = service.login(name, password);
        //user.ifPresent(u -> request.setAttribute("user", u.getLogin()));
        if (optUser.isPresent()) {
            HttpSession session = request.getSession(true);
            User user = optUser.get();
            if (user.getIsActive()) {
                session.setAttribute(USER_ATTR, user);
                request.setAttribute(ERROR_MESSAGE_ATTR, AUTHORIZATION_ERROR_MESSAGE);
                return CommandResult.redirect(MAIN_PAGE_COMMAND);
            } else {
                request.setAttribute(ERROR_MESSAGE_ATTR, USER_BLOCKED_MESSAGE);
                return CommandResult.forward(LOGIN_PAGE);
            }

        } else {
            request.setAttribute(ERROR_MESSAGE_ATTR, AUTHORIZATION_ERROR_MESSAGE);
            return CommandResult.forward(LOGIN_PAGE);
        }
    }
}
