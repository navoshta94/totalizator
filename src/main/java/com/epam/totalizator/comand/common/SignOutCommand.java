package com.epam.totalizator.comand.common;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SignOutCommand implements Command {
    private static final String LOGIN_PAGE = "/WEB-INF/pages/login.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return CommandResult.forward(LOGIN_PAGE);
    }
}
