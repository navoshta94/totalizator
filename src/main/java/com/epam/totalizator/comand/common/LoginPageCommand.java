package com.epam.totalizator.comand.common;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginPageCommand implements Command {
    private final static String LOGIN_PAGE = "/WEB-INF/pages/login.jsp";
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        return CommandResult.forward(LOGIN_PAGE);
    }
}
