package com.epam.totalizator.comand.common;

import com.epam.totalizator.comand.Command;
import com.epam.totalizator.comand.CommandResult;
import com.epam.totalizator.entity.Competition;
import com.epam.totalizator.exceptions.ServiceException;
import com.epam.totalizator.service.CompetitionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MainPageCommand implements Command {
    private static final String COMPETITION_LIST = "competitionsList";
    private static final String MAIN_PAGE = "/WEB-INF/pages/main.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String message = request.getParameter("message");
        request.setAttribute("message", message);
        CompetitionService service = new CompetitionService();
        List<Competition> list = service.acquireCompetitionList();
        request.setAttribute(COMPETITION_LIST, list);
        return CommandResult.forward(MAIN_PAGE);
    }
}
