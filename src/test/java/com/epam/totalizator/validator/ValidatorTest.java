package com.epam.totalizator.validator;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {
    private static final String VALID_ID = "12345";
    private static final String INVALID_ID = "-12345";

    @Test
    public void shouldReturnTrueWhenReceiveValidID() {

        boolean isValid = Validator.validateID(VALID_ID);

        Assert.assertTrue(isValid);
    }

    @Test
    public void shouldReturnFalseWhenReceiveNullID() {
        boolean isValid = Validator.validateID(null);

        Assert.assertFalse(isValid);
    }

    @Test
    public void shouldReturnFalseWhenReceiveInvalidID() {
        boolean isValid = Validator.validateID(INVALID_ID);

        Assert.assertTrue(isValid);
    }
}
