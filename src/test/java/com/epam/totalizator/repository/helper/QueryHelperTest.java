package com.epam.totalizator.repository.helper;


import com.epam.totalizator.repository.helper.QueryHelper;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryHelperTest {
    private static final String TABLE_NAME = "name";
    private static final String FIRST_FIELD = "firstField";
    private static final String SECOND_FIELD = "secondField";
    private static final long ID = 1;
    private static final String EXPECTED_ADD_QUERRY = "insert into name(firstField,secondField)" +
            " values(?,?)";
    private static final String EXPECTED_SAVE_QUERRY = "update name set firstField=?,secondField=?" +
            " where id=1";

    private Map<String, Object> fields = new LinkedHashMap<>();

    @Test
    public void ShouldReturnExpectedAddQueryWhenReceiveMapOfFieldsAndTableName() {
        fields.put(FIRST_FIELD, new Object());
        fields.put(SECOND_FIELD, new Object());

        String actualAddQuery = QueryHelper.makeAddQuery(fields, TABLE_NAME);

        Assert.assertEquals(EXPECTED_ADD_QUERRY, actualAddQuery);
    }

    @Test
    public void ShouldReturnExpectedSaveByIDQueryWhenReceiveMapOfFieldsTableNameAndID() {
        fields.put(FIRST_FIELD, new Object());
        fields.put(SECOND_FIELD, new Object());

        String actualSaveQuery = QueryHelper.makeSaveByIDQuery(fields, TABLE_NAME, ID);

        Assert.assertEquals(EXPECTED_SAVE_QUERRY, actualSaveQuery);
    }
}
