package com.epam.totalizator.utils;

import com.epam.totalizator.entity.Result;
import com.epam.totalizator.entity.bet.Bet;
import com.epam.totalizator.entity.bet.BetKind;
import org.junit.Assert;
import org.junit.Test;

public class WinBetDefinerTest {
    private static final Integer RESULT_FIRST_COMPETITOR_SCORE = 1;
    private static final Integer RESULT_SECOND_COMPETITOR_SCORE = 2;
    private static final Integer FIRST_COMPETITOR_DIFFERENT_SCORE = 0;
    private static final Integer SECOND_COMPETITOR_DIFFERENT_SCORE = 0;

    private Result result = new Result(null, RESULT_FIRST_COMPETITOR_SCORE, RESULT_SECOND_COMPETITOR_SCORE);
    private WinBetDefiner winBetDefiner = new WinBetDefiner();

    @Test
    public void shouldReturnFalseWhenGetBetAndResultWithDifferentScore() {
        Bet differScoreBet = new Bet(null, null, BetKind.EXACT_SCORE,
                FIRST_COMPETITOR_DIFFERENT_SCORE, SECOND_COMPETITOR_DIFFERENT_SCORE,
                null, null);

        boolean isWin = winBetDefiner.isWinBet(result, differScoreBet);

        Assert.assertFalse(isWin);

    }

    @Test
    public void shouldReturnTrueWhenGetBetAndResultWithSameScore() {
        Bet sameScoreBet = new Bet(null, null, BetKind.EXACT_SCORE,
                RESULT_FIRST_COMPETITOR_SCORE, RESULT_SECOND_COMPETITOR_SCORE,
                null, null);

        boolean isWin = winBetDefiner.isWinBet(result, sameScoreBet);

        Assert.assertTrue(isWin);

    }

    @Test
    public void shouldReturnFalseWhenGetBetAndResultWithDifferentResult() {
        Bet differResultBet = new Bet(null, null, BetKind.EXACT_RESULT,
                true, false, false,
                null, null);

        boolean isWin = winBetDefiner.isWinBet(result, differResultBet);

        Assert.assertFalse(isWin);
    }

    @Test
    public void shouldReturnTrueWhenGetBetAndResultWithSameResult() {
        Bet differResultBet = new Bet(null, null, BetKind.EXACT_RESULT,
                false, false, true,
                null, null);

        boolean isWin = winBetDefiner.isWinBet(result, differResultBet);

        Assert.assertTrue(isWin);
    }
}
