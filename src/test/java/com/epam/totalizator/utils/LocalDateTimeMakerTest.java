package com.epam.totalizator.utils;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class LocalDateTimeMakerTest {
    private static final String DATE = "03.03.2017";
    private static final String TIME = "12:30";
    private static final LocalDateTime expected = LocalDateTime.parse("2017-03-03T12:30:00");


    @Test
    public void shouldReturnCorrectLocalDateTimeObjectWhenReceiveDataAndTimeInString() {
        LocalDateTimeMaker localDateTimeMaker = new LocalDateTimeMaker();

        LocalDateTime actualLocalDateTime = localDateTimeMaker.makeLocalDateTime(DATE, TIME);

        Assert.assertEquals(expected.getYear(), actualLocalDateTime.getYear());
        Assert.assertEquals(expected.getMonth(), actualLocalDateTime.getMonth());
        Assert.assertEquals(expected.getDayOfMonth(), actualLocalDateTime.getDayOfMonth());
        Assert.assertEquals(expected.getHour(), actualLocalDateTime.getHour());
        Assert.assertEquals(expected.getMinute(), actualLocalDateTime.getMinute());
        Assert.assertEquals(expected.getSecond(), actualLocalDateTime.getSecond());
    }
}
