$(document).ready(function(){
    $("#en").click(function(){
        var url = window.location.href;
		var index = url.indexOf("sessionLocale");
		if(index > -1) {
			url = url.slice(0, index-1);
		}
		if (url.indexOf('?') > -1 ){
			url += '&sessionLocale=en'
		}else{
			url += '?sessionLocale=en'
		}
		$("#en").attr("formaction", url);
    });
	$("#ru").click(function(){
        var url = window.location.href;
		var index = url.indexOf("sessionLocale");
		if(index > -1) {
			url = url.slice(0, index-1);
		}
		if (url.indexOf('?') > -1){
			url += '&sessionLocale=ru'
		}else{
			url += '?sessionLocale=ru'
		}
		$("#ru").attr("formaction", url);
    });
});