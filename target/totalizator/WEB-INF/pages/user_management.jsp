
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>User management</title>
    <link rel="stylesheet" href="css/user-form-style.css">
    <link rel="stylesheet" href="css/page-style.css">
</head>
<body>
<div class="container">
    <div class="nav">
        <jsp:include page="../fragments/navigation.jsp"/>
    </div>
    <div class="right-column">
        <div class="header">
            <jsp:include page="../fragments/header.jsp"/>
        </div>
        <div class="main">
            <div class="user-table">
                <div class="title">
                    <span class="login"><fmt:message key="user.management.userName"/></span>
                    <span class="status"><fmt:message key="user.management.status"/></span>
                    <span class="role"><fmt:message key="user.management.role"/></span>
                    <span class="actions"><fmt:message key="user.management.actions"/></span>
                </div>
                <div class="users">
                    <c:forEach items="${users}" var="user">
                    <div class="user">
                        <span class="login">${user.login}</span>
                        <span class="status">
                            <c:choose>
                                <c:when test="${user.isActive}">
                                    <fmt:message key="user.management.active"/>
                                </c:when>
                                <c:when test="${not user.isActive}">
                                    <fmt:message key="user.management.blocked"/>
                                </c:when>
                            </c:choose>
                        </span>
                        <span class="role">${user.role}</span>
                        <div class="actions">
                            <form class="button" action="${pageContext.servletContext.contextPath}/controller?command=changeUserStatus" method="POST">
                                <input type="hidden"  name="isActivate" value="false"/>
                                <button  name="userID" value="${user.id}"><fmt:message key="user.management.block"/></button>
                            </form>
                            <form class="button" action="${pageContext.servletContext.contextPath}/controller?command=changeUserStatus" method="POST">
                                <input type="hidden"  name="isActivate" value="true"/>
                                <button  name="userID" value="${user.id}"><fmt:message key="user.management.unblock"/></button>
                            </form>
                        </div>
                    </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</body>
</html>