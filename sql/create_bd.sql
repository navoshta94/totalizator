create database totalizator;
use totalizator;

create table user(
	id bigint not null auto_increment,
    primary key(id),
    login varchar(50),
    password char(32),
    cash_amount decimal,
    role enum ('client', 'admin', 'bookmaker'),
    is_active bit default true
);

create table competition(
	id bigint not null auto_increment,
    primary key(id),
    competition_beginning datetime,
	first_competitor varchar(50),
    second_competitor varchar(50)
);

create table bet_event(
	id bigint primary key not null auto_increment,
    fk_competition_id bigint,
    foreign key(fk_competition_id) references competition(id),
    event_type enum("win_first_competitor", "win_second_competitor", "draw", "exact_score"),
    coefficient decimal(10, 2)
);

create table bet(
	id bigint primary key not null auto_increment,
    fk_competition_id bigint,
    foreign key(fk_competition_id) references competition(id),
    fk_user_id bigint,
    foreign key(fk_user_id) references user(id),
    bet_kind enum("exact_result", "exact_score"),
    first_win bit,
    second_win bit,
    draw bit,
    first_competitor_score int,
    second_competitor_score int,
    bet_amount decimal(5, 2),
    win_bet_amount decimal(8, 2),
    is_win bit
);

create table result(
	id bigint primary key not null auto_increment,
    fk_competition_id bigint,
    foreign key(fk_competition_id) references competition(id),
    first_competitor_score int,
    second_competitor_score int
)

