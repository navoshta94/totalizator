use totalizator;

insert into user(login, password, role)
values("admin", md5("12345"), "admin");

insert into user(login, password, role, cash_amount)
values("client", md5("qwerty007"), "client", 100);

insert into user(login, password, role)
values("bookmaker", md5("12345"), "bookmaker");

insert into competition(first_competitor, second_competitor, competition_beginning)
values("Chelsea", "Barcelona", '2019-01-01 18:00:00');

insert into competition(first_competitor, second_competitor, competition_beginning)
values("Roma", "Real Madrid", '2019-01-02 18:00:00');

insert into competition(first_competitor, second_competitor, competition_beginning)
values("PSG", "Liverpool", '2019-01-03 18:00:00');


